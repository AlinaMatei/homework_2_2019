function addTokens(input, tokens){
    
        if(typeof input !== 'string'){
            throw new Error('Invalid input');
        }
        if(input.length < 6){
            throw new Error('Input should have at least 6 characters');
        }

        if (tokens.some(x => typeof x === "string") === true && Array.isArray(tokens)) {
            throw new Error("Invalid array format");
        }
        
        if(!(input.includes('...'))){
            return input;
        }
        let i = 0;        
        return input.replace(('...'), '${' + Object.values(tokens[i++]) + '}');        
    
}

const app = {
    addTokens: addTokens
}

module.exports = app;

